const components = (import.meta as any).globEager('./components/**/*.vue');
const componentsMap = {};
Object.keys(components).forEach((key) => {
  const component: any = components[key];
  componentsMap[component?.default?.__name || component?.default?.name] = component.default;
});
export default componentsMap;

export const useComponentRegister = (compName: any, component: any) => {
  componentsMap[compName] = component.default;
};
